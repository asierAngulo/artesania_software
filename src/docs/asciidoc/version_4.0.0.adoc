[.opaque]
== Recapitulemos...
image::marx_hed.jpg[background, size=auto auto]

[.opaque]
=== ...partimos de...
[ditaa]
....
+--------+     +----------+      +----------+      +------------+      +-------+      +----------------------+      +-----+
|        |     |          |      |          |      |            |      |       |      |                      |      |     |
| Inicio +---> | Análisis +----> | Diseño   +----> | Desarrollo +----> | Tests +----> | Puesta en producción +----> | Fin |
|        |     |          |      |          |      |            |      |       |      |                      |      |     |
+--------+     +----------+      +----------+      +------------+      +-------+      +----------------------+      +-----+

t --->
....

image::modern_times_serie.jpg[background, size=cover]

[.opaque]
=== ...pero en realidad
image::artilleria.jpg[background, size=auto 100%]

[.opaque]
=== !
image::software_development_cycle.jpg[]
{scrum} {kanban}

image::scrum.jpg[background, size=cover]

[.opaque]
=== !
image::bdd.jpg[]

{behaviour_driven_development}

image::scrum.jpg[background, size=cover]

[.opaque]
=== https://agilemanifesto.org/[#agile]
image::tools.png[background, size=cover]

[.opaque]
=== !
image::tools.png[background, size=cover]

==== Individuals and interactions
over processes and tools

==== Working software
over comprehensive documentation

==== Customer collaboration
over contract negotiation

==== Responding to change
over following a plan


